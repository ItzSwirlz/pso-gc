NAME := pso-gc

BUILD_DIR := build/$(NAME)

DOL := $(BUILD_DIR)/PsoV3.dol
ELF := $(DOL:.dol=.elf)

WINE := wine
export WINEDEBUG ?= -all
DEVKITPPC := /opt/devkitpro/devkitPPC

MWCC_VERSION := 1.3.2
MWLD_VERSION := 1.3.2

ASM_INPUTS := $(wildcard asm/*.s)
C_INPUTS := $(wildcard src/*.c)
CPP_INPUTS := $(wildcard src/*.cpp)
LDSCRIPT := $(BUILD_DIR)/ldscript.lcf

AS := $(DEVKITPPC)/bin/powerpc-eabi-as
CPP := $(DEVKITPPC)/bin/powerpc-eabi-cpp -P

CC := $(WINE) tools/mwcc_compiler/$(MWCC_VERSION)/mwcceppc.exe
LD := $(WINE) tools/mwcc_compiler/$(MWCC_VERSION)/mwldeppc.exe

INCLUDES := -i include -i include/dolphin -i include/libc
ASM_INCLUDES := -I include/

ifdef LD_TEST
TEST_LD := -DLD_TEST
else
TEST_LD :=
endif

LDFLAGS := -fp hard -nodefaults

ASFLAGS := -mgekko $(ASM_INCLUDES)

CFLAGS_BASE := -I- $(INCLUDES) $(TEST_LD) -nodefaults -proc gekko -fp hard -O4 -use_lmw_stmw on -str reuse -rostr -sdata2 4

CFLAGS := $(CFLAGS_BASE)
CXXFLAGS := $(CFLAGS_BASE) -Cpp_exceptions off -RTTI off

include obj_files.mk

# This is a hack, and should be removed once we make separate repos for
# all the support libraries (such as the Dolphin SDK, MSL, and MetroTRK).
$(BUILD_DIR)/src/pso/%.o: DIR_UNIQUE_CFLAGS := -inline none -sym on

# File specific flags.

ALL_DIRS := $(sort $(dir $(O_FILES)))

default: all

all: $(ELF)

DUMMY != mkdir -p $(ALL_DIRS)

$(LDSCRIPT): ldscript.lcf
	$(CPP) -MMD -MP -MT $@ -MF $@.d -I include/ -I . -DBUILD_DIR=$(BUILD_DIR) -o $@ $<

clean:
	rm -f -d -r build
	find . -name '*.o' -exec rm {} +

$(ELF): $(O_FILES) $(LDSCRIPT)
	@echo $(O_FILES) > build/o_files
	$(LD) $(LDFLAGS) -o $@ -lcf $(LDSCRIPT) @build/o_files

$(BUILD_DIR)/%.o: %.s
	@echo Assembling $<
	$(QUIET) $(AS) $(ASFLAGS) -o $@ $<

$(BUILD_DIR)/%.o: %.c
	@echo "Compiling " $<
	$(QUIET) $(CC) -lang=c $(CFLAGS) $(DIR_UNIQUE_CFLAGS) $(FILE_UNIQUE_CFLAGS) -c -o $@ $<

$(BUILD_DIR)/%.o: %.cp
	@echo "Compiling " $<
	$(QUIET) $(CC) $(CXXFLAGS) $(DIR_UNIQUE_CFLAGS) $(FILE_UNIQUE_CFLAGS) -c -o $@ $<

$(BUILD_DIR)/%.o: %.cpp
	@echo "Compiling " $<
	$(QUIET) $(CC) -lang=c++ $(CXXFLAGS) $(DIR_UNIQUE_CFLAGS) $(FILE_UNIQUE_CFLAGS) -c -o $@ $<
