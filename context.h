// Compiler version:
// 2.4.2 build 81 (GC MW 1.3.2)

// Compiler flags:
// -nodefaults -proc gekko -fp hard -O4 -use_lmw_stmw on -enum int -str reuse -rostr -sdata2 4 -lang=c++ -Cpp_exceptions off -RTTI off

// System typedefs.
typedef signed char s8;
typedef signed short int s16;
typedef signed long s32;
typedef signed long long int s64;
typedef unsigned char u8;
typedef unsigned short int u16;
typedef unsigned long u32;
typedef unsigned long long int u64;
typedef int BOOL;

typedef unsigned long size_t;
typedef long ptrdiff_t;

// Macros.
#define FALSE 0
#define TRUE 1

#define NULL 0
#if !defined(__cplusplus) || __cplusplus < 201103L
#ifndef nullptr
#define nullptr NULL
#endif
#endif
#define ATTRIBUTE_ALIGN(num) __attribute__((aligned(num)))

// pso/macros.h
#define EXTERN_OBJECT_NAME(name) extern const char *name##_name;

#define OBJECT_NAME(name) const char *name##_name = #name;
#define OBJECT_NAMES \
	o(TObject) \
	o(TMainTask)
#define TL_OBJECTS \
	o(tl_su, TL_SU) \
	TL_OBJECTS_AT_TL_00

#define TL_OBJECTS_AT_TL_00 \
	TL_00_OBJECTS \
	TL_02_OBJECTS \
	o(tl_particle, TL_PARTICLE) \
	TL_PARTICLE_MODEL_OBJECTS

#define TL_OBJECTS_AT_TL_00_WITHOUT_TL_PARTICLE \
	TL_00_OBJECTS \
	TL_02_OBJECTS \
	TL_PARTICLE_MODEL_OBJECTS

#define TL_OBJECTS_AT_TL_02 \
	TL_02_OBJECTS \
	o(tl_particle, TL_PARTICLE) \
	TL_PARTICLE_MODEL_OBJECTS

#define TL_00_OBJECTS \
	o(tl_00, TL_00) \
	o(tl_camera, TL_CAMERA) \
	o(tl_01, TL_01)

#define TL_02_OBJECTS \
	o(tl_02, TL_02) \
	o(tl_item_equip, TL_ITEM_EQUIP) \
	o(tl_03, TL_03) \
	o(tl_loc_start, TL_LOC_START) \
	o(tl_04, TL_04) \
	o(tl_05, TL_05) \
	o(tl_06, TL_06) \
	o(tl_07, TL_07) \
	o(tl_loc_end, TL_LOC_END) \
	o(tl_window, TL_WINDOW)

#define TL_PARTICLE_MODEL_OBJECTS \
	o(tl_particlemodel, TL_PARTICLEMODEL) \
	o(tl_radermap, TL_RADERMAP) \
	o(tl_clipout, TL_CLIPOUT) \
	o(tl_fade, TL_FADE) \
	o(tl_fadeafter, TL_FADEAFTER)

#define FOREACH_NODE(type, first, varname) for (type *varname = (type *)(first); varname != NULL; varname = (type *)(varname->next()))
#define FOREACH_NODE_NODECL(type, first, varname) for (varname = (type *)(first); varname != NULL; varname = (type *)(varname->next()))

#define FOREACH_NODE_MULTI_ITER(type, first, varname, ...) for (type *varname = (type *)(first); varname != NULL; varname = (type *)(varname->next()), __VA_ARGS__)
#define FOREACH_NODE_NODECL_MULTI_ITER(type, first, varname, ...) for (varname = (type *)(first); varname != NULL; varname = (type *)(varname->next()), __VA_ARGS__)

#define __packed__
#define WEAK_FUNC __declspec(weak)

#define PRIVATE_MEMBER_GETTER(type, name)	\
	type name() {				\
		return m_##name;		\
	}

#define PRIVATE_MEMBER_SETTER(type, name)	\
	void set_##name(type val) {		\
		m_##name = val;			\
	}

#define PRIVATE_MEMBER_GETTER_ARRAY(type, name, size)	\
	TArray<type, size> &name() {			\
		return m_##name;			\
	}

#define PRIVATE_MEMBER_GETTER_C_ARRAY(type, name, size)	\
	TArray<type, size> &name() {			\
		return to_TArray<type, size>(m_##name);	\
	}

#define PRIVATE_MEMBER_GETTER_FUNC(ret_type, name, ...)	\
	ret_type (*name())(__VA_ARGS__) {		\
		return m_##name;			\
	}

#define PRIVATE_MEMBER_SETTER_FUNC(ret_type, name, ...)		\
	void set_##name(ret_type (*val)(__VA_ARGS__)) {		\
		m_##name = val;					\
	}

#define PRIVATE_MEMBER_ACCESSORS(type, name)	\
	PRIVATE_MEMBER_GETTER(type, name);	\
	PRIVATE_MEMBER_SETTER(type, name)

#define PRIVATE_MEMBER_ACCESSORS(type, name)	\
	PRIVATE_MEMBER_GETTER(type &, name);	\
	PRIVATE_MEMBER_SETTER(type, name)

#define PRIVATE_MEMBER_ACCESSORS_NON_REF(type, name)	\
	PRIVATE_MEMBER_GETTER(type, name);		\
	PRIVATE_MEMBER_SETTER(type, name)

#define PRIVATE_MEMBER_ACCESSORS_ARRAY(type, name, size)	\
	PRIVATE_MEMBER_GETTER_ARRAY(type, name, size)

#define PRIVATE_MEMBER_ACCESSORS_C_ARRAY(type, name, size)	\
	PRIVATE_MEMBER_GETTER_C_ARRAY(type, name, size)

#define PRIVATE_MEMBER_ACCESSORS_FUNC(ret_type, name, ...)		\
	PRIVATE_MEMBER_GETTER_FUNC(ret_type, name, __VA_ARGS__);	\
	PRIVATE_MEMBER_SETTER_FUNC(ret_type, name, __VA_ARGS__)

#define as(type, var) reinterpret_cast<type>(var)

// pso/TMainTask.h
#define X_OR_Y_CHILD(flags, old_flags, one_prefix, zero_prefix, suffix)					\
	if (flags != old_flags) {									\
		TMainTask *child;									\
		u32 bit = 1;										\
		FOREACH_NODE_NODECL_MULTI_ITER(TMainTask, main_task.m_down, child, bit <<= 1) {		\
			if (flags & bit) {								\
				child->one_prefix##_##suffix();						\
			} else {									\
				child->zero_prefix##_##suffix();					\
			}										\
		}											\
		old_flags = flags;									\
	}

#define SET_OR_CLEAR_CHILD_FLAGS(flags, old_flags, flag_bit) \
	X_OR_Y_CHILD(flags, old_flags, set_flag, clear_flag, flag_bit)

#define DISALLOW_OR_ALLOW_CHILD(flags, old_flags, flag_name) \
	X_OR_Y_CHILD(flags, old_flags, allow, disallow, flag_name)

// pso/TProtocol.h
#define RECV_PACKET_HANDLERS \
	o(handle_unused_login, void) \
	o(handle_03_recv_regist, u8 state) \
	o(handle_04_recv_login, u8 error_code) \
	o(handle_05_recv_logout, void) \
	o(handle_07_A0_A1_recv_dir_list, int entry_count, GameListEntry *entries, GameListEntry &entry_0) \
	o(handle_08_recv_game_list, int entry_count, GameListEntry *entries, GameListEntry &entry_0) \
	o(handle_01_recv_error, char *mesg) \
	o(handle_06_recv_chat, TPlyGuildCardTag &tag, char *mesg) \
	o(handle_11_recv_message, char *mesg, u8 unused) \
	o(handle_1A_D5_recv_text, char *text) \
	o(handle_0E_recv_start_game, TPlyJoinLobbyData *entries, u8 entry_count, int lobby_entry_idx) \
	o(unused7, void) \
	o(handle_64_recv_start_game3, TPlyJoinGame &join_data) \
	o(unused8, void) \
	o(handle_67_recv_start_lobby2, TPlyJoinLobbyEntry *entries, u8 entry_count, int client_id, int leader_id, int lobby_number, int block_number, int smth, int event) \
	o(handle_80_recv_generate_id, TRecvGenerateID gen_id) \
	o(unused9, void) \
	o(handle_65_recv_burst_game, TPlyJoinLobbyEntry *entries, int leader_id, int disable_udp) \
	o(handle_66_recv_exit_game, int client_id, int leader_id, int disable_udp) \
	o(handle_68_recv_burst_lobby, TPlyJoinLobbyEntry *entries, int leader_id) \
	o(handle_69_recv_exit_lobby, int client_id, int leader_id) \
	o(handle_18_90_9A_recv_pso_regist_check, u8 state) \
	o(handle_92_9C_register_response_packet, void) \
	o(unused10, void) \
	o(handle_95_request_character_data_packet, void) \
	o(handle_81_recv_chat_message, TChatMessage &chat_message) \
	o(handle_41_recv_user_ans, TUserAns &user_ans) \
	o(send_96_unused, void) \
	o(handle_97_checksum_reply_packet, void) \
	o(handle_B1_current_time_packet, void) \
	o(handle_C0_choice_search_option_packet, void) \
	o(handle_C4_choice_search_reply_packet, void) \
	o(handle_D8_infoboard_packet, void) \
	o(handle_A2_recv_quest_menu_list, TMenuList<QuestListEntry, 30, 0> &quest_list) \
	o(handle_A3_recv_quest_menu_info, TPlyText<288> &info) \
	o(handle_44_recv_download_head, TRecvDownloadHead &download_head) \
	o(handle_13_recv_download, TRecvDownload &download) \
	o(handle_A4_recv_quest_list, u8 entry_count, TMenuList<QuestListEntry, 30, 0> &quest_list) \
	o(handle_A5_recv_quest_info, TPlyText<288> &info) \
	o(handle_A6_recv_vm_download_head, TRecvDownloadHead &download_head) \
	o(handle_A7_recv_vm_download, TRecvDownload &download) \
	o(unused11, void) \
	o(handle_1F_recv_text_list, int entry_count, GameListEntry *entries, GameListEntry &entry_0) \
	o(handle_B0_recv_emergency_call, char *mesg) \
	o(handle_88_player_arrow_color_list_packet, void) \
	o(handle_8A_lobby_name_packet, void) \
	o(handle_C5_player_challenge_data_packet, void) \
	o(handle_DA_lobby_event_packet, void) \
	o(handle_AB_quest_stats_response_packet, void) \
	o(handle_D3_execute_trade_packet, void) \
	o(handle_D4_trade_result_packet, void) \
	o(handle_D1_advance_trade_state_packet, void)

#define recv_packet_handler(name) recv_packet_handler_##name

// pso/TMenuList.h
#define TMenuListEntry(name, members)	\
class name {				\
public:					\
	void bswap() {			\
		tag.bswap();		\
	};				\
public:					\
	TPlyGuildCardTag tag;		\
	members				\
} __packed__

// pso/packet_classes.h
#if !defined(__GNUC__) && !defined(__clang__)
#define _TSendAction(action_type, members)	\
template<>					\
class TSendAction<action_type> {		\
public:						\
	packet_header header;			\
	TPlyGuildCardTag tag;			\
	members					\
public:						\
	void bswap() {				\
		header.bswap();			\
		tag.bswap();			\
	};					\
}
#endif

// pso/TPlyDispData.h
#define bswap16(var) bswap_16(as(u16 *, var))
#define bswap32(var) bswap_32(as(u32 *, var))

#define bswap16_ref(var) bswap16(&var)
#define bswap32_ref(var) bswap32(&var)

// pso/forward.h
// Class forward.
class TTcpSocket;
class PSOV3EncryptionTCP;
class TSocket;
class THeap;
class TObject;
class TMainTask;
class TPlyGuildCardTag;
class TProtocol;
struct packet;
struct packet_header;

// pso/TProtocol.h
// User typedefs.
#define o(name, ...) typedef void (*recv_packet_handler(name))(__VA_ARGS__);
RECV_PACKET_HANDLERS;
#undef o
typedef void (TProtocol::*command_handler)(packet &pkt);

// pso/TArray.h
// Template defs.
template<typename T, size_t n>
class TArray {
public:
	T m_data[n];

public:
	T *data() {
		return m_data;
	};

	size_t size() {
		return n;
	};

	size_t byte_size() {
		return n * sizeof(T);
	};

	T &at(size_t i) {
		return m_data[i];
	}

	T &operator[](size_t i) {
		return at(i);
	};

	T *start() {
		return m_data;
	};

	T *end() {
		return &m_data[size()-1];
	};

	template<typename T2>
	T2 *as() {
		return reinterpret_cast<T2 *>(m_data);
	};

	template<typename T2>
	T2 &at_as(size_t i) {
		return reinterpret_cast<T2>(at(i));
	};

	template<typename T2>
	size_t size_as() {
		return byte_size() / sizeof(T2);
	};

	template<typename T2>
	TArray<T2, (n * sizeof(T))/sizeof(T2)> &to() {
		typedef TArray<T2, sizeof(m_data)/sizeof(T2)> to_type;
		return reinterpret_cast<to_type &>(*this);
	};

	u8 *as_bytes() {
		return reinterpret_cast<u8 *>(m_data);
	};

	void fill(u8 val) {
		memset(m_data, val, byte_size());
	};

	template<typename T2>
	void fill_with(const T2 val) {
		_fill_with<T2, false>(val);
	};

	template<typename T2>
	void fast_fill_with(const T2 val) {
		_fill_with<T2, true>(val);
	};

	template<typename T2>
	void copy(const T2 &val) {
		_copy<T2, false>(val);
	};

	template<typename T2>
	void copy_reverse(const T2 &val) {
		_copy_reverse<T2, false>(val);
	};

	template<typename T2>
	void fast_copy(const T2 &val) {
		_copy<T2, true>(val);
	};

	template<typename T2>
	void fast_copy_reverse(const T2 &val) {
		_copy_reverse<T2, true>(val);
	};

private:
	template<typename T2, bool do_unroll_check>
	void _fill_with(const T2 val) {
		T2 *data = as<T2>();
		size_t size = size_as<T2>();
		if (do_unroll_check && size <= 8) {
			int i = 0;
			switch (size) {
				case 8: data[i++] = val;
				case 7: data[i++] = val;
				case 6: data[i++] = val;
				case 5: data[i++] = val;
				case 4: data[i++] = val;
				case 3: data[i++] = val;
				case 2: data[i++] = val;
				case 1: data[i++] = val;
				default: break;
			}
		} else {
			for (int i = 0; i < size; ++i) {
				data[i] = val;
			}
		}
	};

	template<typename T2, bool do_unroll_check>
	void _copy(const T2 &val) {
		size_t size = sizeof(T2)/sizeof(T);
		const T *src = reinterpret_cast<const T *>(&val);
		if (do_unroll_check && size <= 8) {
			int i = 0;
			switch (size) {
				case 8: m_data[i] = src[i++];
				case 7: m_data[i] = src[i++];
				case 6: m_data[i] = src[i++];
				case 5: m_data[i] = src[i++];
				case 4: m_data[i] = src[i++];
				case 3: m_data[i] = src[i++];
				case 2: m_data[i] = src[i++];
				case 1: m_data[i] = src[i++];
				default: break;
			}
		} else {
			for (int i = 0; i < size; ++i) {
				m_data[i] = src[i];
			}
		}
	};

	template<typename T2, bool do_unroll_check>
	void _copy_reverse(const T2 &val) {
		size_t size = sizeof(T2)/sizeof(T);
		const T *src = reinterpret_cast<const T *>(&val);
		if (do_unroll_check && size <= 8) {
			int i = size-1;
			int j = 0;
			switch (size) {
				case 8: m_data[i--] = src[j++];
				case 7: m_data[i--] = src[j++];
				case 6: m_data[i--] = src[j++];
				case 5: m_data[i--] = src[j++];
				case 4: m_data[i--] = src[j++];
				case 3: m_data[i--] = src[j++];
				case 2: m_data[i--] = src[j++];
				case 1: m_data[i--] = src[j++];
				default: break;
			}
		} else {
			for (int i = size-1, j = 0; i; --i, ++j) {
				m_data[i] = src[j];
			}
		}
	};
};

template<typename T, size_t n>
inline TArray<T, n> &to_TArray(T *array) {
	return reinterpret_cast<TArray<T, n> &>(*array);
}

// pso/TMenuList.h
template <typename T, int num_entries, int num_pad_entries>
class TMenuList {
public:
	void bswap() {
		header.bswap();
		if (num_pad_entries) {
			for (int i = 0; i < num_pad_entries; i++) {
				pad_entries[i].bswap();
			}
		}
		if (num_entries) {
			for (int i = 0; i < num_entries; i++) {
				entries[i].bswap();
			}
		}
	};
public:
	packet_header header;
	T pad_entries[num_pad_entries];
	T entries[num_entries];
} __packed__;

#ifdef __MWERKS__
template <typename T, int num_entries>
class TMenuList<T, num_entries, 0> {
public:
	void bswap() {
		header.bswap();
		for (int i = 0; i < num_entries; i++) {
			entries[i].bswap();
		}
	};
public:
	packet_header header;
	T entries[num_entries];
} __packed__;

template <typename T, int num_pad_entries>
class TMenuList<T, 0, num_pad_entries> {
public:
	void bswap() {
		header.bswap();
		for (int i = 0; i < num_pad_entries; i++) {
			pad_entries[i].bswap();
		}
	};
public:
	packet_header header;
	T pad_entries[num_pad_entries];
} __packed__;
#endif

// pso/packet_classes.h
template<size_t size>
struct TPlyText {
	packet_header header;
	char text[size];

	void bswap() {
		header.bswap();
	};
};

TMenuListEntry(GameListEntry,
	u8 difficulty_tag;
	u8 num_players;
	char name[16];
	u8 episode;
	u8 flags;
);

TMenuListEntry(LobbyListEntry,
	u32 smth;
);

TMenuListEntry(QuestListEntry,
	char name[32];
	char short_description[112];
);

class TBlockedSenders {
public:
	packet_header header;
	u32 blocked_senders[30];
public:
	void bswap() {
		header.bswap();
		for (int i = 0; i < 30; ++i) {
			bswap_32(&blocked_senders[i]);
		}
	};
};

class TPlyJoinData {
public:
	packet_header header;
	TPlyCharData char_data;
	TPlyChallenge challenge;
	TPlyChoiceSearchConfig choice_search_config;
	char info_board[172];
	u32 blocked_senders[30];
	u32 auto_reply_enabled;
	char auto_reply[512];
public:
	void bswap() {
		header.bswap();
		char_data.bswap();
		challenge.bswap();
		choice_search_config.bswap();
		for (int i = 0; i < 30; ++i) {
			bswap_32(&blocked_senders[i]);
		}
		bswap_32(&auto_reply_enabled);
	};
};

template<int action_type = 0>
class TSendAction {
public:
	packet_header header;
	TPlyGuildCardTag tag;
	#if defined(__GNUC__) || defined(__clang__)
	char name[(action_type & 1) ? 16 : 0];
	char password[(action_type & 2) ? 16 : 0];
	#endif
public:
	void bswap() {
		header.bswap();
		tag.bswap();
	};
};

#if !defined(__GNUC__) && !defined(__clang__)
_TSendAction(1,
	char name[16];
);

_TSendAction(2,
	char password[16];
);

_TSendAction(3,
	char name[16];
	char password[16];
);

#undef _TSendAction
#endif

// pso/TProtocol.h
// NOTE: This is only here because of the ordering of this.
//       In Reality, the definition of `_packet` is above `flex_packet`.
struct _packet;

template<size_t n = 0>
union flex_packet {
	struct _packet pkt;
	u8 bytes[(n) ? n : sizeof(packet_header)];
};

// TMainTask.cpp
// Const defs.
static const int tl_object_count = 20;

// TTcpSocket.cpp
// Extern defs.
EXTERN_OBJECT_NAME(TTcpSocket);
extern TTcpSocket *tcp_socket_table[16];

// pso/TProtocol.h
EXTERN_OBJECT_NAME(TProtocol);
extern int enable_v2_features;
extern u32 new_ip_addr;
extern u16 new_port;
extern TPlyMeetUserExtension meet_user_settings;
extern u32 game_variations[16][2];

// TObject.cpp
#define o(name) extern const char *name##_name;
OBJECT_NAMES
#undef o

// TMainTask.cpp
#define o(var, name) extern const char *var##_name;
TL_OBJECTS
#undef o

#define o(var, name) extern TObject *var;
TL_OBJECTS
#undef o

// THeap.cpp
extern THeap *obj_heap;
extern THeap *alt_heap;
// TObject.cpp
extern TMainTask main_task;
extern TObject global_obj1;
extern TObject global_obj2;

// TMainTask.cpp
// Variable defs.
u32 some_main_task_flag = 1;
u32 update_flags;
u32 old_update_flags;
u32 render_flags;
u32 old_render_flags;
u32 render_shadow_flags;
u32 old_render_shadow_flags;
u32 some_id_805c6f74;

// TTcpSocket.cpp
// func defs.
extern void func_80019aa0();
extern void controller_stuff();
extern char func_801a5d1c();
extern void render_tick();
extern void func_803d96a4();
extern short tcp_abort(short nh);
extern short tcp_bind(short nh, struct at_ip_addr *addr, u16 port);
extern short tcp_connect(short nh, struct at_ip_addr *addr, u16 port, struct at_ip_option *option);
extern short tcp_create();
extern short tcp_delete(short nh);
extern short tcp_get_opt(short nh, short type, u32 *opt);
extern short tcp_send(short nh, void (*notify)(short size, short sock_fd), char bufnum, struct send_buffs *sb);
extern short tcp_stat(short nh, short *stat, short *backlog, u32 *sendwin, u32 *recvwin);
extern short tcp_receive(short nh, void (*notify)(short size, short sock_fd), short len, u8 *buf);
extern int get_link_status();
extern char *get_sock_status_name(short code);

// pso/TSocket.h
extern u16 to_be_uint16_t(u16 val);
extern u16 to_le_uint16_t(u16 val);

// TMainTask.cpp
extern void camera_stuff();

extern void set_depth_buffer_settings_1();
extern void set_depth_buffer_settings_2();
extern void set_depth_buffer_settings_3();
extern void set_depth_buffer_settings_id(u32 id);
extern void save_depth_buffer_settings();
extern void restore_depth_buffer_settings();

extern void func_80083a00();
extern void func_80141618();
extern void set_some_id(u32 id);
extern void func_803e11e8(u32 arg1);
extern void func_803e11f0();

extern void func_803369b4();

// pso/TProtocol.h
extern void copy_packet(struct packet *pkt);

// pso/protocol.h
extern void bswap_16(u16 *val);
extern void bswap_32(u32 *val);

// Struct defs.
struct packet_header {
	u8 command;
	u8 flags;
	u16 size;
	void bswap() { bswap_16(&size); };
} __packed__;

// pso/TMath.h
struct vec2f {
	float x;
	float y;
};

// pso/TPlyDispData.h
struct stats {
	void bswap();

	short atp;
	short mst;
	short evp;
	short hp;
	short dfp;
	short ata;
	short lck;
};

struct player_stats {
	void bswap() {
		bswap16(&stats.atp);
		bswap16(&stats.mst);
		bswap16(&stats.evp);
		bswap16(&stats.hp);
		bswap16(&stats.dfp);
		bswap16(&stats.ata);
		bswap16(&stats.lck);
		bswap16(&mbr_0xe);
		bswap32(&proportions);
		bswap32(&mbr_0x14);
		bswap32(&level);
		bswap32(&exp);
		bswap32(&meseta);
	};

	struct stats stats;
	short mbr_0xe;
	float proportions;
	float mbr_0x14;
	int level;
	int exp;
	int meseta;
};

struct disp_data_part2 {
	void bswap() {
		bswap_32(&name_colour);
		bswap_32(&name_colour_checksum);
		bswap_32(&v1_flags);
		for (int i = 0; i < 8; ++i) {
			bswap_16(&appearance_data[i]);
		}

		bswap32(&proportions.x);
		bswap32(&proportions.y);
	};
	bool is_valid() {
		// TODO: Actually decompile it.
		return false;
	};

	char name[16];
	u32 smth[2];
	u32 name_colour;
	u8 extra_model;
	u8 padding[15];
	u32 name_colour_checksum;
	u8 section_id;
	u8 char_class;
	u8 v2_flags;
	u8 version;
	u32 v1_flags;
	union {
		struct {
			u16 costume;
			u16 skin;
			u16 face;
			u16 head;
			u16 hair;
			u16 hair_r;
			u16 hair_g;
			u16 hair_b;
		} appearance;
		u16 appearance_data[8];
	};
	vec2f proportions;
};

// pso/TPlyGuildCardTag.h
struct gc_tag {
	gc_tag() { init(); };
	~gc_tag() {};
	void init() {
		tag0 = 0;
		tag1 = 0;
		tag2 = 0;
	};
	void bswap() {
		bswap_16(&tag2);
	};


	u8 tag0;
	u8 tag1;
	u16 tag2;
};

// pso/TPlyInventory.h
struct TItemData {
	void init() {
		data1.data_u8[0] = 0;
		data1.data_u8[1] = 0;
		data1.data_u8[2] = 0;
		data1.data_u8[3] = 0;
		data1.data_u8[4] = 0;
		data1.data_u8[5] = 0;
		data2.data_u32 = 0;
		for (int i = 0; i < 3; ++i) {
			data1.data_u8_pair[3+i][0] = 0;
			data1.data_u8_pair[3+i][1] = 0;
		}
		id = -1;
	};
	void bswap() {
		bswap_32(&data2.data_u32);
		bswap_32(&id);
	};

	union {
		u8 data_u8[12];
		u8 data_u8_pair[6][2];
		u16 data_u16[6];
		u32 data_u32[3];
	} data1;
	u32 id;
	union {
		u8 data_u8[4];
		u8 data_u16[2];
		u32 data_u32;
	} data2;
};

// TTcpSocket.cpp, but they're really from AVE-TCP
struct at_ip4_opt {
	u8 ttl;
	u8 svctype;
	u8 df_flag;
};

struct at_ip6_opt {
	u8 traffic_class;
	u32 flow_label;
	u8 hop_limit;
};

struct at_ip_option {
	u32 type;
	union {
		struct at_ip6_opt ip6;
		struct at_ip4_opt ip4;
	} ip46;
};

struct at_ip_addr {
	u32 type;
	union {
		u8 ip6[16];
		u32 ip4;
	} ip46;
};

struct send_buffs {
	short len;
	u8 *buff;
};

// pso/packet_classes.h
#define _gc_tag_packet_templ(name, members)	\
template<bool has_gc_tag = false>		\
struct name {					\
	packet_header header;			\
	members					\
};						\
						\
template<>					\
struct name<true> {				\
	packet_header header;			\
	TPlyGuildCardTag tag;			\
	members					\
}

_gc_tag_packet_templ(regist,
	TPlySmth smth;
	u32 sub_version;
	u8 is_extended;
	u8 language;
);

struct game_command_header {
	char command;
	char size;
	u16 id;

	void bswap() {
		bswap_16(&id);
	};
};

struct extended_game_command_header {
	game_command_header header;
	u32 size;

	void bswap() {
		bswap_16(&header.id);
		bswap_32(&size);
	};
};

struct long_game_command {
	game_command_header header;
	u8 data[1460-sizeof(game_command_header)];

	void bswap() {
		header.bswap();
	};
};

struct extended_long_game_command {
	extended_game_command_header header;
	u8 data[1460-sizeof(extended_game_command_header)];

	void bswap() {
		header.bswap();
	};
};

struct game_command {
	game_command_header header;
	u8 data[1024-sizeof(game_command_header)];

	void bswap() {
		header.bswap();
	};
};

struct extended_game_command {
	extended_game_command_header header;
	u8 data[1024-sizeof(extended_game_command_header)];

	void bswap() {
		header.bswap();
	};
};

// pso/TProtocol.h
struct _packet {
	void bswap() {
		header.bswap();
	};

	packet_header header;
	u8 data[];
};

struct packet : public flex_packet<0x7c00> {};

struct recv_packet_handlers {
	#define o(name, ...) recv_packet_handler(name) name;
	RECV_PACKET_HANDLERS;
	#undef o
};

struct command_handler_entry {
	int command;
	command_handler handler;
};

// pso/TSocket.h
// Union defs.
union ipv4_addr {
	u32 addr;
	u8 addr_bytes[4];
};

// pso/packet_classes.h
union game_command_union {
	struct game_command game_cmd;
	struct extended_game_command ext_game_cmd;
	u8 bytes[1024];
};

union long_game_command_union {
	struct long_game_command game_cmd;
	struct extended_long_game_command ext_game_cmd;
	u8 bytes[1460];
};

// pso/TObject.h
// Enum defs.
enum object_flags {
	NONE = 0,
	QUEUE_DESTRUCTION = 1,
	CHILD_QUEUE_DESTRUCTION = 2,
	BIT_2 = 4,
	BIT_3 = 8,
	DISALLOW_UPDATE = 0x0F,
	DISALLOW_RENDER = 0x10,
	DISALLOW_DESTRUCTION = 0x20,
	DISALLOW_RENDER_SHADOWS = 0x100,
	BIT_9 = 0x200,
	BIT_10 = 0x400,
	BIT_11 = 0x800,
	BIT_12 = 0x1000,
	BIT_13 = 0x2000,
	BIT_14 = 0x4000,
	BIT_15 = 0x8000,
	ALL_BITS = 0xFFFF
};

// pso/TPlyCharData.h
// Class defs.
class TPlyCharData {
public:
	TPlyCharData() {
		for (int i = 0; i < 30; ++i) {
			m_inventory.m_items[i].init();
		}
	};

	~TPlyCharData() {};
	void bswap() {
		for (int i = 0; i < 30; ++i) {
			m_inventory.m_items[i].bswap();
		}
		m_disp_data.m_disp_part2.bswap();
		m_disp_data.m_stats.bswap();
		m_disp_data.m_config.bswap();
	};
	void some_stub() {};
public:
	TPlyInventory m_inventory;
	TPlyDispData m_disp_data;
};

// pso/TPlyClientConfig.h
class TPlyClientConfig {
public:
public:
	struct {
		u32 magic[2];
		u32 flags;
		u32 proxy_dst_addr;
		u16 proxy_dst_port;
		u8 mbr_0xe[14];
	} config;
};

class TPlyDispConfigSmth {
public:
	TPlyDispConfigSmth() { m_mbr_0x0 = 0; };
	void bswap() {
		bswap_32(&m_mbr_0x0);
		for (int i = 0; i < 4; ++i) {
			m_tags[i].bswap();
			m_tags1[i].bswap();
		}
	};
public:
	u32 m_mbr_0x0;
	gc_tag m_tags[4];
	gc_tag m_tags1[4];
};

class TPlyDispConfig {
public:
	void bswap() {
		m_smth.bswap();
		m_smth1.bswap();
	};
public:
	TPlyDispConfigSmth m_smth;
	TPlyDispConfigSmth m_smth1;
};

class TPlyDispData {
public:
	void bswap();
public:
	player_stats m_stats;
	disp_data_part2 m_disp_part2;
	TPlyDispConfig m_config;
	u8 m_tech_levels[20];
};

// pso/TPlyGuildCardTag.h
class TPlyGuildCardTag {
public:
	TPlyGuildCardTag() {};

	TPlyGuildCardTag(u16 tag2, u32 guildcard_number) {
		m_tag0 = 0;
		m_tag1 = 0;
		m_tag2 = tag2;
		m_guildcard_number = guildcard_number;
	};

	int operator==(TPlyGuildCardTag &other) {
		return (m_tag1 == other.m_tag1) && (m_tag2 == other.m_tag2) && (m_guildcard_number == other.m_guildcard_number);
	};

	void bswap() {
		bswap_32(&m_guildcard_number);
		bswap_16(&m_tag2);
	};

public:
	u8 m_tag0;
	u8 m_tag1;
	u16 m_tag2;
	u32 m_guildcard_number;
} __packed__;

// pso/TPlyInventory.h
class TPlyInventoryItem {
public:
	void init() {
		m_present[0] = 0;
		m_present[1] = -1;
		m_flags = 0;
		m_present[3] = 0;
		m_present[2] = 0;
		m_data.init();
	};
	void bswap() {
		bswap_32(&m_flags);
		m_data.bswap();
	};
public:
	char m_present[4];
	u32 m_flags;
	TItemData m_data;
};

class TPlyInventory {
public:
	void bswap();
public:
	u8 m_num_items;
	u8 m_hp_materials_used;
	u8 m_tp_materials_used;
	u8 m_language;
	TPlyInventoryItem m_items[30];
};

// pso/TPlySmth.h
class TPlySmth {
public:
	TPlySmth() {};

	void bswap() {
		bswap_32(as(u32 *, &m_smth));
		bswap_32(as(u32 *, &m_smth1));
	};
public:
	u8 m_smth[4];
	u8 m_smth1[4];
};

// pso/PSOV3EncryptionTCP.h
class PSOV3EncryptionTCP : public PSOV3Encryption {
public:
	PSOV3EncryptionTCP();
	~PSOV3EncryptionTCP();

	void reset(u32 seed);
	void encrypt(void *void_data, int size);
public:
	u32 m_seed;
};

// pso/THeap.h
class THeap {
public:
	struct heap_node {
		heap_node *next;
		size_t remaining_size;
	};
	heap_node *heap_nodes;
	size_t mbr_0x04;
	size_t align;
	size_t mbr_0x0C;
	size_t mbr_0x10;
public:
	THeap(size_t size, int align);
	~THeap();
	void *operator new(size_t size);
	void operator delete(void *ptr);
	void *heap_alloc(size_t size);
	void *heap_zalloc(size_t size);
	void heap_free(void *ptr);
};

// pso/TObject.h
class TObject {
#ifdef TOBJECT_CPP
	#define _delete_children() {		\
		while (m_down != NULL) {	\
			delete m_down;		\
		}				\
	}
	#define add_parent(parent, set_parent) {		\
		if (set_parent) {				\
			m_up = parent;				\
		}						\
		/*TObject *child;*/				\
		if (parent == NULL) {				\
			m_prev = this;				\
			m_next = NULL;				\
		} else {					\
			TObject *child = parent->m_down;	\
			if (child != NULL) {			\
				m_prev = child->m_next;		\
				m_next = NULL;			\
				child->m_prev->m_next = this;	\
				child->m_prev = this;		\
			} else {				\
				m_prev = this;			\
				parent->m_down = this;		\
				m_next = NULL;			\
			}					\
		}						\
	}
	#define remove_parent() {					\
		if (m_up != NULL) {					\
			if (m_prev == this) {				\
				m_up->m_down = NULL;			\
			} else if (m_up->m_down == this) {		\
				m_up->m_down = m_next;			\
				m_prev->m_next = NULL;			\
				if (m_next != NULL) {			\
					m_next->m_prev = m_prev;	\
				}					\
			} else {					\
				m_prev->m_next = m_next;		\
				if (m_next != NULL) {			\
					m_next->m_prev = m_prev;	\
				} else {				\
					m_up->m_down->m_prev = m_prev;	\
				}					\
			}						\
		}							\
	}
#endif
public:
	const char *m_name;
	u16 m_flags;
	u16 m_id;
	TObject *m_prev;
	TObject *m_next;
	TObject *m_up;
	TObject *m_down;
public:
	void allow_rendering_shadows() { m_flags &= ~DISALLOW_RENDER_SHADOWS; };
	void disallow_rendering_shadows() { m_flags |= DISALLOW_RENDER_SHADOWS; };

	void clear_flag_9() { m_flags &= ~BIT_9; };
	void set_flag_9() { m_flags |= BIT_9; };
	u32 get_flag_9() { return m_flags & BIT_9; };

	void allow_rendering() { m_flags &= ~DISALLOW_RENDER; };
	void disallow_rendering() { m_flags |= DISALLOW_RENDER; };

	void clear_flag_3() { m_flags &= ~BIT_3; };
	void set_flag_3() { m_flags |= BIT_3; };
	void toggle_flag_3() { m_flags ^= BIT_3; };

	void queue_destruction() { m_flags |= QUEUE_DESTRUCTION; };

	TObject(TObject *parent = NULL);
	virtual ~TObject();

	void *operator new (size_t size);
	void operator delete(void *ptr);

	void delete_children();
	void queue_destruction_for_each_node();
	void run_tasks();
	void render_nodes();
	void render_shadows_for_each_node();
	void render_nodes2();
	void empty_func();
	void set_parent(TObject *parent);

	virtual void run_task();
	virtual void render();
	virtual void render_shadows();

	void empty_func2();
	void log(const char *str);
	int get_node_count();
	int all_parents_unqueued_for_destruction();
	bool toggle_flag_9_if_flag_10_is_clear();
};

// pso/TSocket.h
class TSocket : public TObject {
public:
	ipv4_addr m_dst_addr;
	u16 m_dst_port;
	u16 m_src_port;
	ipv4_addr m_src_addr;
	s16 m_sock_fd;
	char m_is_invalid_packet;
	char m_buffer_cleared;
	s16 m_size;
	s16 m_buffer_offset;
	u32 m_unused;
	u8 m_unused2[64];
	u8 m_packet_buffer[2048];
	s16 m_stat_val;
	u16 m_unused3;
	u32 m_send_window;
	u32 m_recv_window;
	void (*m_callback)(TSocket *socket);
public:
	TSocket(TObject *parent);
	virtual ~TSocket();

	virtual int open() = 0;
	virtual short close() = 0;
	virtual void recv() = 0;
	virtual short send(u8 *data) = 0;
	virtual short send(u8 *data, size_t size) = 0;

	int resolve_domain(char *domain);
	void set_ip_address(u32 addr);
	void set_port(u32 port);
	const u8 next();
	int is_empty();
};

// pso/TTcpSocket.h
class TTcpSocket : public TSocket {
public:
	PSOV3EncryptionTCP m_send_crypt;
	PSOV3EncryptionTCP m_recv_crypt;
	int m_is_encrypted;
public:
	TTcpSocket(TObject *parent) : TSocket(parent) {
		m_name = TTcpSocket_name;
	}

	virtual ~TTcpSocket();

	virtual int open();
	virtual short close();
	virtual void recv();
	virtual short send(u8 *data);
	virtual short send(u8 *data, size_t size);

	short stat();
	void some_stub();
	int test_connection();

	static void notify(short size, short sock_fd);
};

// pso/TMainTask.h
class TMainTask : public TObject {
public:
	u32 task_flags;
	u32 mbr_0x20;
	u32 mbr_0x24;
public:
	TMainTask();
	void render_screen_overlay();
	void some_empty_func();
	void tl_toggle_flag_3();
	void tl_clear_flag_3();
	void tl_delete_children();
	void tl_02_toggle_flag_3();
	void tl_02_clear_flag_3();
	void empty_render_screen_overlay_func();
	void run_tl_camera_tasks();
	void unused_render_func();
	void render_objects();
	void render_ui();
	void render_particle_effects();
	void render_effects();
	void render_geometry();
	void func_80228bbc();
	void func_80228c44(s32 arg0);
	void func_80228dbc();
	void render_clipout_and_fade();
	void init_main_task();

	virtual ~TMainTask();
	virtual void run_task();
	virtual void render();
	virtual void render_shadows();
};

// pso/PSOV3Encryption.h
class PSOEncryption {
public:
	PSOEncryption();
	virtual void update_stream() = 0;
	virtual ~PSOEncryption();
	virtual void init(u32 seed) = 0;
	virtual u32 next() = 0;
};

class PSOV3Encryption : public PSOEncryption {
public:
	PSOV3Encryption();
	virtual void update_stream();
	virtual ~PSOV3Encryption();
	virtual void init(u32 seed);
	virtual u32 next();
public:
	u32 m_buffer[522];
	u32 *m_buffer_start;
	u32 *m_buffer_end;
};

// pso/packet_classes.h
class TCreateGame {
public:
	packet_header header;
	TPlyGuildCardTag tag;
	char name[16];
	char password[16];
	u8 difficulty;
	u8 battle_mode;
	u8 challenge_mode;
	u8 episode;
public:
	void bswap() {
		header.bswap();
		tag.bswap();
	};
};

class TFindUser {
public:
	packet_header header;
	TPlyGuildCardTag tag;
	u32 target_gc;
public:
	void bswap() {
		header.bswap();
		tag.bswap();
		bswap_32(&target_gc);
	};
};

class TRoomChange {
public:
	packet_header header;
	TPlyGuildCardTag tag;
public:
	void bswap() {
		header.bswap();
		tag.bswap();
	};
};

class TSendPsoRegist : public regist<false> {
public:
	char serial_number[48];
	char access_key[48];
	char password[48];
public:
	void bswap() {
		header.bswap();
		smth.bswap();
		bswap_32(&sub_version);
	};
};

class TSendPsoRegistConnect {
public:
	packet_header header;
	char serial_number[17];
	char access_key[17];
	u16 padding;
public:
	void bswap() {
		header.bswap();
	};
};

class TRecvPsoRegistConnect {
public:
	packet_header header;
	char copyright[64];
	u32 server_key;
	u32 client_key;
public:
	void bswap() {
		header.bswap();
		bswap_32(&server_key);
		bswap_32(&client_key);
	};
};

template<bool is_lobby>
class TRecvExit {
public:
	packet_header header;
	char client_id;
	char leader_id;
	char disable_udp;
public:
	void bswap() {
		header.bswap();
	};
};

class TRegister : public regist<false> {
public:
	char serial_number[16];
	char access_key[16];
public:
	void bswap() {
		header.bswap();
		smth.bswap();
		bswap_32(&sub_version);
	};
};

class TRecvPort {
public:
	packet_header header;
	u32 ip_addr;
	short port;
public:
	void bswap() {
		header.bswap();
		bswap_32(&ip_addr);
		bswap_16(as(u16 *, &port));
	};
};

class TPlyMeetUserExtension {
public:
	TPlyGuildCardTag tags[8];
	u32 mbr_0x40;
	char player_name[16];
	char player_name2[16];
public:
	void bswap() {
		for (int i = 0; i < 8; ++i) {
			tags[i].bswap();
		}
		bswap_32(&mbr_0x40);
	};
};

class TChatMessage {
public:
	packet_header header;
	TPlyGuildCardTag tag;
	char name[16];
	u32 to_guildcard_number;
	char text[512];
public:
	void bswap() {
		header.bswap();
		tag.bswap();
		bswap_32(&to_guildcard_number);
	};
};

class TUserAns {
public:
	packet_header header;
	TPlyGuildCardTag tag;
	u32 found_guildcard;
	TRecvPort recv_port;
	char location[68];
	TPlyMeetUserExtension extension;
public:
	void bswap() {
		header.bswap();
		tag.bswap();
		bswap_32(&found_guildcard);
		recv_port.bswap();
		extension.bswap();
	};
};

class TGenGuildCardTag {
public:
	u8 tag0;
	u8 tag1;
	u16 tag2;
	u32 guildcard_number;
public:
	TGenGuildCardTag() {};

	void bswap() {
		bswap_32(&guildcard_number);
		bswap_16(&tag2);
	};
};

class TRecvGenerateID {
public:
	packet_header header;
	u32 client_id;
	TGenGuildCardTag tag;
public:
	void bswap() {
		header.bswap();
		bswap_32(&client_id);
		tag.bswap();
	};
};

class TPsoDataBuf {
public:
	void recv_pso_data_buf(size_t size) {};
};

class TPsoDataLong : public TPsoDataBuf {
public:
	union {
		union long_game_command_union pkt;
		u8 bytes[0x8000];
	};
};

class TPsoData : public TPsoDataBuf {
public:
	union game_command_union pkt;
};

class TRecvPsoDataLong {
public:
	packet_header header;
	TPsoDataLong data;
public:
	void bswap() {
		header.bswap();
	};
};

class TRecvPsoData {
public:
	packet_header header;
	TPsoData data;
public:
	void bswap() {
		header.bswap();
	};
};

class TPlyJoinLobbyData {
public:
	TPlyGuildCardTag tag;
	u32 ip_addr;
	u32 client_id;
	char name[16];
public:
	void bswap() {
		tag.bswap();
		bswap_32(&ip_addr);
		bswap_32(&client_id);
	};
};

class TPlyJoinLobbyEntry {
public:
	TPlyJoinLobbyData lobby_data;
	TPlyCharData char_data;
public:
	void bswap() {
		lobby_data.tag.bswap();
		bswap_32(&lobby_data.ip_addr);
		bswap_32(&lobby_data.client_id);
		char_data.bswap();
	};
};

class TRecvDownload {
public:
	packet_header header;
	char filename[16];
	u8 data[1024];
	u32 data_size;
public:
	void bswap() {
		header.bswap();
		bswap_32(&data_size);
	};
};

class TSendDownloadHead {
public:
	packet_header header;
	char filename[16];
public:
	void bswap() {
		header.bswap();
	};
};

class TRecvDownloadHead {
public:
	packet_header header;
	char path[32];
	u16 unused;
	u16 flags;
	char filename[16];
	u32 file_size;
public:
	void bswap() {
		header.bswap();
		bswap_32(&file_size);
	};
};

class TPlyJoinLobby {
public:
	packet_header header;
	char client_id;
	char leader_id;
	char disable_udp;
	char lobby_number;
	char block_number;
	char unknown1;
	char event;
	char unknown2;
	char unused[4];
	TPlyJoinLobbyEntry entries[12];
public:
	void bswap() {
		header.bswap();
		for (int i = 0; i < 12; ++i) {
			entries[i].bswap();
		}
	};
};

class TPlyJoinGame {
public:
	packet_header header;
	u32 variations[16][2];
	TPlyJoinLobbyData lobby_data[4];
	u8 client_id;
	u8 leader_id;
	u8 disable_udp;
	u8 difficulty;
	u8 battle_mode;
	u8 event;
	u8 section_id;
	u8 challenge_mode;
	u32 rare_seed;
	u8 episode;
	u8 unknown1;
	u8 solo_mode;
	u8 unknown2;
public:
	void bswap() {
		header.bswap();
		for (int i = 0; i < 16; ++i) {
			for (int j = 0; j < 2; ++j) {
				bswap_32(&variations[i][j]);
			}
		}
		bswap_32(&rare_seed);
	};
};

class TPlyStartGame {
public:
	packet_header header;
	TPlyJoinLobbyData lobby_data[4];
	TPlyMeetUserExtension extension;
	u8 mbr_0xe8[4];
public:
	void bswap() {
		header.bswap();
		for (int i = 0; i < 4; ++i) {
			lobby_data[i].bswap();
		}
		extension.bswap();
	};
};

class TPlyGuildCardTagPacket {
public:
	packet_header header;
	TPlyGuildCardTag tag;
};

class TPlyRecvLogin : public TPlyGuildCardTagPacket {
public:
	TPlyClientConfig client_config;
public:
	void bswap() {
		header.bswap();
		tag.bswap();
	};
};

class TMessageBox {
public:
	packet_header header;
	TPlyGuildCardTag tag;
	char mesg[512];
public:
	void bswap() {
		header.bswap();
		tag.bswap();
	};
};

// pso/TProtocol.h
class TProtocol : public TTcpSocket {
public:
	TPlySmth m_smth;
	TPlyGuildCardTag m_guildcard_tag;
	u16 m_sub_version;
	int m_regist_state;
	int m_connected;
	int m_joined_game;
	int m_has_meet_user_settings;
	int m_handle_pings_only;
	int m_entry_count;
	int m_unused;
	int m_lobby_list_count;
	int m_unused2;
	int m_udp_disabled;
	int m_language;
	u8 m_unused3[80];
	TPlyCharData m_character_data;
	u32 m_unused4;
	GameListEntry m_game_entries[64];
	GameListEntry m_game_entries2[64];
	LobbyListEntry m_lobby_entries[16];
	QuestListEntry m_quest_entries[30];
	u8 m_unused5[128];
	char m_serial_number_v1[17];
	char m_access_key_v1[17];
	char m_serial_number[48];
	char m_access_key[48];
	char m_password[64];
	char m_player_name[16];
	char m_serial_number2[17];
	char m_access_key2[17];
	TPlyClientConfig m_client_config;
	int m_packet_offset;
	int m_packet_size;
	struct packet m_packet;
	struct recv_packet_handlers m_recv_handlers;
private:
	static void operator delete(void *ptr) { obj_heap->heap_free(ptr); };
public:
	TProtocol(TObject *parent, u16 sub_version, int language, char *serial_number, char *access_key, char *password);
	virtual ~TProtocol();

	virtual void run_task();
	virtual short send(u8 *data, size_t size);

	void some_stub();
	int handle_command(struct packet *pkt);
	void parse_packet();
	void seq_jump(u8 *some_struct, TPlyMeetUserExtension &extension);

	// Command handlers.
	// 0x01
	void recv_error(packet &pkt);
	// 0x1A/0xD5
	void recv_text(packet &pkt);
	// 0x03
	void recv_regist(packet &pkt);
	// 0x04
	void recv_login(packet &pkt);
	// 0x05
	void recv_logout(packet &pkt);
	// 0x06
	void recv_chat(packet &pkt);
	// 0x07/0xA0/0xA1
	void recv_dir_list(packet &pkt);
	// 0x08
	void recv_game_list(packet &pkt);
	// 0x1F
	void recv_text_list(packet &pkt);
	// 0x0E
	void recv_start_game(packet &pkt);
	// 0x64
	void recv_start_game3(packet &pkt);
	// 0x67
	void recv_start_lobby2(packet &pkt);
	// 0x12
	void recv_banner(packet &pkt);
	// 0x16
	void recv_banner_head(packet &pkt);
	// 0x44
	void recv_download_head(packet &pkt);
	// 0xA6
	void recv_vm_download_head(packet &pkt);
	// 0xA7
	void recv_vm_download(packet &pkt);
	// 0x13
	void recv_download(packet &pkt);
	// 0x14
	void recv_upload(packet &pkt);
	// 0x11
	void recv_message(packet &pkt);
	// 0x19
	void recv_port(packet &pkt);
	// 0x1B
	void recv_battle_data(packet &pkt);
	// 0x1C
	void recv_system_file(packet &pkt);
	// 0x60/0x62
	void recv_pso_data(packet &pkt);
	// 0x6C/0x6D
	void recv_pso_data_long(packet &pkt);
	// 0x80
	void recv_generate_id(packet &pkt);
	// 0x83
	void recv_room_info(packet &pkt);
	// 0x41
	void recv_user_ans(packet &pkt);
	// 0x1D
	void recv_ping(packet &pkt);
	// 0x81
	void recv_chat_message(packet &pkt);
	// 0xA2
	void recv_quest_menu_list(packet &pkt);
	// 0xA3
	void recv_quest_menu_info(packet &pkt);
	// 0xA4
	void recv_quest_list(packet &pkt);
	// 0xA5
	void recv_quest_info(packet &pkt);
	// 0xB0
	void recv_emergency_call(packet &pkt);
	// 0x65
	void recv_burst_game(packet &pkt);
	// 0x66
	void recv_exit_game(packet &pkt);
	// 0x68
	void recv_burst_lobby(packet &pkt);
	// 0x69
	void recv_exit_lobby(packet &pkt);
	// 0x91
	void recv_pso_regist_connect(packet &pkt);
	// 0x18/0x90
	void recv_pso_regist_check(packet &pkt);

	// Send command handlers.
	// 0x03
	void send_regist();
	// 0x04
	void send_login2(int disable_udp);
	// Doesn't send a command.
	int send_login3();
	// 0x05
	void send_logout();
	// 0x06
	void send_chat(TPlyGuildCardTag &tag, char *mesg);
	// 0x10
	void send_action(GameListEntry &selection, char *action_name, char *password);
	void send_action(GameListEntry &selection, u8 flags, char *action_name, char *password);
	// 0x09
	void send_info(TPlyGuildCardTag &tag);
	// 0x08
	void send_game_list();
	// 0x1F
	void send_text_list();
	// 0x61
	void send_chara_data_v2(TPlyCharData &char_data, TPlyChallenge &challenge, TPlyChoiceSearchConfig &choice_search_config, TBlockedSenders &blocked_senders, TPlyText<512> &auto_reply, char *info_board);
	// 0x98
	void send_update_chara_data_v2(TPlyCharData &char_data, TPlyChallenge &challenge, TPlyChoiceSearchConfig &choice_search_config, TBlockedSenders &blocked_senders, TPlyText<512> &auto_reply, char *info_board);
	// 0x60
	void send_pso_data(game_command &packet, u32 size);
	// 0x62
	void send_pso_data2(u32 flags, game_command &packet, u32 size);
	// 0x6D
	void send_pso_data_long2(char flags, long_game_command &packet, size_t size);
	// 0x84
	void send_room_change(int idx);
	// 0x40
	void send_find_user(TPlyGuildCardTag &tag, u32 gc_num);
	// 0xC1
	void send_create_game(char *name, char *password, u8 difficulty, u8 battle_mode, u8 challenge_mode, u8 episode);
};
