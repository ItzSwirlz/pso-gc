#ifndef MACROS_H
#define MACROS_H

#define EXTERN_OBJECT_NAME(name) extern const char *name##_name;

#define OBJECT_NAME(name) const char *name##_name = #name;
#define OBJECT_NAMES \
	o(TObject) \
	o(TMainTask)
#define TL_OBJECTS \
	o(tl_su, TL_SU) \
	TL_OBJECTS_AT_TL_00

#define TL_OBJECTS_AT_TL_00 \
	TL_00_OBJECTS \
	TL_02_OBJECTS \
	o(tl_particle, TL_PARTICLE) \
	TL_PARTICLE_MODEL_OBJECTS

#define TL_OBJECTS_AT_TL_00_WITHOUT_TL_PARTICLE \
	TL_00_OBJECTS \
	TL_02_OBJECTS \
	TL_PARTICLE_MODEL_OBJECTS

#define TL_OBJECTS_AT_TL_02 \
	TL_02_OBJECTS \
	o(tl_particle, TL_PARTICLE) \
	TL_PARTICLE_MODEL_OBJECTS

#define TL_00_OBJECTS \
	o(tl_00, TL_00) \
	o(tl_camera, TL_CAMERA) \
	o(tl_01, TL_01)

#define TL_02_OBJECTS \
	o(tl_02, TL_02) \
	o(tl_item_equip, TL_ITEM_EQUIP) \
	o(tl_03, TL_03) \
	o(tl_loc_start, TL_LOC_START) \
	o(tl_04, TL_04) \
	o(tl_05, TL_05) \
	o(tl_06, TL_06) \
	o(tl_07, TL_07) \
	o(tl_loc_end, TL_LOC_END) \
	o(tl_window, TL_WINDOW)

#define TL_PARTICLE_MODEL_OBJECTS \
	o(tl_particlemodel, TL_PARTICLEMODEL) \
	o(tl_radermap, TL_RADERMAP) \
	o(tl_clipout, TL_CLIPOUT) \
	o(tl_fade, TL_FADE) \
	o(tl_fadeafter, TL_FADEAFTER)

#define FOREACH_NODE(type, first, varname) for (type *varname = (type *)(first); varname != NULL; varname = (type *)(varname->m_next))
#define FOREACH_NODE_NODECL(type, first, varname) for (varname = (type *)(first); varname != NULL; varname = (type *)(varname->m_next))

#define FOREACH_NODE_MULTI_ITER(type, first, varname, ...) for (type *varname = (type *)(first); varname != NULL; varname = (type *)(varname->m_next), __VA_ARGS__)
#define FOREACH_NODE_NODECL_MULTI_ITER(type, first, varname, ...) for (varname = (type *)(first); varname != NULL; varname = (type *)(varname->m_next), __VA_ARGS__)

#define __packed__
#define WEAK_FUNC __declspec(weak)

#define PRIVATE_MEMBER_GETTER(type, name)	\
	type name() {				\
		return m_##name;		\
	}

#define PRIVATE_MEMBER_SETTER(type, name)	\
	void set_##name(type val) {		\
		m_##name = val;			\
	}

#define PRIVATE_MEMBER_GETTER_ARRAY(type, name, size)	\
	TArray<type, size> &name() {			\
		return m_##name;			\
	}

#define PRIVATE_MEMBER_GETTER_C_ARRAY(type, name, size)	\
	TArray<type, size> &name() {			\
		return to_TArray<type, size>(m_##name);	\
	}

#define PRIVATE_MEMBER_GETTER_FUNC(ret_type, name, ...)	\
	ret_type (*name())(__VA_ARGS__) {		\
		return m_##name;			\
	}

#define PRIVATE_MEMBER_SETTER_FUNC(ret_type, name, ...)		\
	void set_##name(ret_type (*val)(__VA_ARGS__)) {		\
		m_##name = val;					\
	}

#define PRIVATE_MEMBER_ACCESSORS(type, name)	\
	PRIVATE_MEMBER_GETTER(type &, name);	\
	PRIVATE_MEMBER_SETTER(type, name)

#define PRIVATE_MEMBER_ACCESSORS_NON_REF(type, name)	\
	PRIVATE_MEMBER_GETTER(type, name);		\
	PRIVATE_MEMBER_SETTER(type, name)

#define PRIVATE_MEMBER_ACCESSORS_ARRAY(type, name, size)	\
	PRIVATE_MEMBER_GETTER_ARRAY(type, name, size)

#define PRIVATE_MEMBER_ACCESSORS_C_ARRAY(type, name, size)	\
	PRIVATE_MEMBER_GETTER_C_ARRAY(type, name, size)

#define PRIVATE_MEMBER_ACCESSORS_FUNC(ret_type, name, ...)		\
	PRIVATE_MEMBER_GETTER_FUNC(ret_type, name, __VA_ARGS__);	\
	PRIVATE_MEMBER_SETTER_FUNC(ret_type, name, __VA_ARGS__)

#define as(type, var) reinterpret_cast<type>(var)

#endif
